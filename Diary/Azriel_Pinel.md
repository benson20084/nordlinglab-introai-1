Diary Entry 1 (03/22/2020) 
* It was interesting to read some of the success stories of Artificial Intelligence. 
*As a psychology major , i thought it would be interesting to research how AI has out performed 
humans in psychotherapy. 
* Ai therapy systems have proven to be highly successful. There have been many break throughs beginning with 
Eliza in the 1960's through principles of Cognitive Behavioral Therapy (CBT).
* Ai therapy systems help people who struggle with anxiety and depression , and veterans suffering from PTSD 
using webcam and microphone which enables the system to analyze emotional cues and provide appropriate feedback.


>>>> 03/29/2020
Today's reading has definitely broadened my perspective of Ai and what encompasses the whole concept of Ai. 
My previous understanding of the definition of Ai was centered on the general idea that Ai is what humans use to make
life easier for ourselves, and to accomplish things we otherwise are physically or even mentally incapable of.

Last week's lecture was interesting and very thought provoking. It was interesting getting to know some of my classmate's thinking about certain
things regarding Ai. I think that we somehow need to have a stable footing when it comes to how much we allow Ai to do for us because the consequences
can be very detrimental.