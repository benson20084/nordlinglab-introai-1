

This diary file is written by Zonghan Wu E94086050 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* AI can help solve problems that humans can't accomplish.
* It turns out that AI is almost ubiquitous in our daily life.Media,website,online games,parking lot,bank,......,
  we're now coexisting with AI.
* AI can be classify into severral levels,from common automatic control to deep learning.
* Even though AI is everwhere,I actually know few of them like how it learns,why it is different from ordinary machine. 

# 2020-03-28 #

* It is not easy to give AI a clear definition, but we can express characteristic properties of AI by two words-Autonomy and Adaptivity.
* What seems easy to humans(like grabing something) can be complicated to robots.On the other hand, what seems hard to us(like playing chess) can be easy to robots.
* Computer science[ AI[ Machine learning[ Deep learning ] ] ]
* The Turing test is not completed because Turing just constrained the set of behaviors into discussion but ignoring how they make decision.
* The intelligent behavior doesn't meanbeing actually intelligent.
* It's surprising that AGI has been all but abandoned by the AI researchers and they see no progress toward it for 50 years!

# 2020-04-11 #

* I spent three days to get used to the fundamental operation of PowerShell.This is the hardest part because it's totally new to me.
* After knowing how to use PowerShell,I kept studying the fist 6chapter, and the syntax is comparely easy to me.
## notes for use of PowerShell ##

* pwd :print working firectory.
* cd :change directory.
* cd ~ :back home.
* cd ..:move "up" in the tree and path.
* mkdir :make directory.
* ls :list what you have in this folder.
* rmdir :remove directory.
* pushd :namely "Save where I am,and go there."
* popd :pop off the last directory you pushed( use pushd ) and take you back there.
* New-Item :make empty files or directories.
* cp :copy files or folders.
* mv :move files or rename them.
* more :to view files.
*　cat :to view files.
* rm :remove files or folders.
* exit :exit the terminal.
## notes for python ##

* print : print something
* #(comments) :anything after # is ignored python

# 2020-04-19 #

* I still study python with the website "Learn python the hard way".However, I found that I must pay to see more or just by the book.
* Now I've found a series of video teaching python in Youtube and Iintend to buy a book about python.
## notes for python ##

* strings : put either " or ' around the text and form the strings.
* strings can contain any number of variables that are declared.
* .format() 
* end='' :this can make python printing without new line

# 2020-04-26 #

* It turns out that the tutorials had been uploaded. I learn some python syntax and do the exerise in the Drive.
* Now I'm still checking what Tensorflaw works for and not download yet.I'm still confused about what the code in "Nordling Lab - Python and Deep learning tutorial using TensorFlow 2.0 in Colaboratory" means.
  Maybe it's because I stil don't understant python or what.

