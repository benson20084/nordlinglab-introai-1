This diary file is written by Kevin Chen F44076055 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* Acquired the information of this course.
* A good start from a beneficial journey.
* Learned about the importance of AI, and its applications.

# 2019-02-27 #

* Learned about the difference among some similar termonologies, such as AI, Machine learning, Data mining, etc.    
* Communicated with new friends about today's topic and extracted different opinions from them.
* After the class, I learned much more about AI by googling.
* Because I was a python programmer, I hope we can learn AI by using python in class.
* I learned lots of new knowledge that I didn't understand before, like GPU.

# 2019-03-06 #

* Learned about the sytax of python, such as how to print, how to use function.
* Also learned some advanced notion, like class, inheritance.
* And learned editing the diary in a correct way.

# 2019-03-13 #

* Learning the syntax of python by programming
* It is the best way to learn programming by this way!

# 2019-03-20 #

* Known the first thing about neural network, such as the purpose, the mechanism, etc.
* I don't understand a lot of terms, such as CNN, hidden layer, etc.
* I might take a look about the art of NN after this class.

# 2019-03-27 #
* Learned about the concept of classification and regression e.g.how to make it become more accurate, etc.
* Learned what is overfitting and underfitting
* Learned the reason why the model is always wrong, which can be depicted by the fundamental principle of uncertainty.
* Homework: Watch a video about neural network before class

# 2019-04-17 #
* Learned about the concept of tensor.
* Learned what is mnist dataset.
* Learned the simple sytaxes of Tensorflow based on python.
* Homework: read and figure out the meaning of each line in the example program.

# 2019-04-24 #
* Learned Tensorflow by exercising.(Using google colab).

# 2019-05-01 #
* Reviewed Tensorflow by exercising(Teacher explain each line of the code thoroughly)
* Make lots of discussion with classmates for Tensorflow, makes us know about it more

# 2019-05-08 #
* Reviewed all the notions(such as Gradient descent and Backpropagation,Convolutional Neural Networks)
* Prof. answered our some of our puzzling question
* I think this lesson was the best lesson because it make me review every concept I've learned in the class,
  Thank you, dear professor:)

# 2019-05-16 #
* Introduced the background of the development of AI
* Learned that AI might be use to assist our work, or even replace us in what way
* The most important of all is Prof. conclusion, saying that if we can collaborate with the tech(such as AI) in a more 
  concerted way. We won't be affraid of being fired!
  
# 2019-05-22 #
<<<<<<< HEAD
* Learned the ethical issue of the application of AI
* Known that we should use technology in a good way, which improves our lives!

# 2019-05-29 #
* We discussed about our presentation(e.g. which model, what data)

# 2019-06-13 #
* I learned new knowledge about C
NN by listening to peers presentation, hope that
wwe can do as well as the group s presented today
=======
* Learned about the ethical issue of AI application.
* Know not to misuse AI and another technology.
>>>>>>> origin/Kevin-Chen/kevin_chenmd-edited-online-with-bitbucke-1558539351344
