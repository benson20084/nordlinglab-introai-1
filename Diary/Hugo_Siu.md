This diary file is written by Hugo Siu E14087092 in the course "Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists".

# 2020-03-22 #

* This is the first time that I have interacted more deeply with Bitbucket. The first time I entered the platform I found it very confusing because I did not know what I had to do to catch up with the course.
* Unfortunately everything has been altered due to this pandemic that today is affecting worldwide. The fact that the class has switched to online mode changed my perspective of the course, because is not the same, you can not interact in the same way as witnessing a class in person.
* This afternoon I was watching some videos about AI and I am very  fascinated by the idea of making machines think like humans. Of all the examples I found, one of the ones that caught my attention the most is: Manufacturing and drone robots.
* I think everyone should watch this video about AI, it is very interesting (https://www.youtube.com/watch?v=s0dMTAQM4cw)
* There is something I am not that sure about and it is the ppt we need to upload. I hope I can find the TA tomorrow so I can ask for help.
* I am very excited to learn a lot from this course, I wish the class was not online, so I could interact with the hole class in person.

# 2020-03-29 #
* This week I was researching about Python and learned a lot.
* Python is a dynamic typed interpreted programming language whose philosophy emphasizes syntax that favors readable code. It is a multi-paradigm programming language and available on various platforms.
* By using a readable syntax, the learning curve is very fast, making it one of the best languages to start programming in text mode. For example, if we compare code written in block programming language like blockly and write the same code using Python.
* Python contains a large number of libraries, data types and functions built into the language itself, which help you perform many common tasks without having to program them from scratch. But what really makes it brilliant using it on a Raspberry Pi is the ability to use GPIO pins to connect the physical world to the digital world.
* Python has a command line interpreter where you can enter statements. Each statement is executed and produces a visible result, which can help us better understand the language and test the results of executing portions of code quickly.
* It has many functions incorporated in the language itself, for the treatment of strings, numbers, files and more. In addition, there are many libraries that we can import into programs to deal with specific topics such as programming windows or networked systems or things as interesting as creating compressed .zip files.
* A simple readable and elegant language that follows a set of rules that makes your learning curve very short. If you already have some notions of programming or you come from programming in other languages such as Java, it will not be difficult for you to start reading and understanding the code developed in Python.
* I found this video very interesting, I think we all should watch it (https://www.youtube.com/watch?v=Y8Tko2YC5hA)

# 2020-04-06 #
* This weekend I did some research on The neural network models
* Neural networks are computational models loosely inspired by the behavior observed in their biological conuterpart.
* It consists of a set of units, called artificial neurons, connected together to transmit signals. The input information passes through the neural network( where it undergoes various operations) producing output values.
* One of the websites that supports me to undertand a little about this topic is the following (https://www.sas.com/en_ca/insights/analytics/neural-networks.html)

# 2020-04-12 #
* I watched the video about TensorFlow (https://www.youtube.com/watch?v=u4alGiomYP4&feature=youtu.be)
* This is an open source software library for numerical computing that used data flow graphs. The nodes in the graphs represent mathematical operations, while the endges of the graphs represent the multidimensional data communicated between them.
* The health sector is one of the fields that is being revolutionized the most and that will have the greatest impact for all of us as a society in the coming years
* TensorFlow is already improving the tools doctors use, for example by helping them analyze radiographs.
* Deep Learning will allow medical preactitioners to spend more time with patients, as well as allowing them to do more interesting and exciting activities.
* TensorFlow was built with open source and ease of execution and scalability in mind. It can be run in the cloud, but also locally.
* The idea is that anyone can run it. You can see people running it on a single machine, a single device, a single CPU (or GPU), or in large clusters. The disparity is very high.

# 2020-04-19 #
* We had our first online class, it was interesting to know how the course can be.
* I feel much better when having online classes, instead of just reading.
* I watched the video  about the introduction to recognition of handwritten digits using the MNIST example in "TensorFlor and Deep Learning without a PhD" by Martin Gorner.
* I learned how to setup Colab to utilise GPU for computations.
* I feel very excited for the next class, see what we are going to learn and know each other a little bit more.

# 2020-04-26 #
* This week I learned a lot about data preprocesing
* Data processing occurs when data is collected and translated into usable information.
* Data scientists are often involved, alone or in teams, and it is important that the processing is done ccorrectly so as not adversely affect the final product or the results obtained from the data.
* Processing starts with data in its raw form adn converts it to a more readble format, giving it the form and context necessary for computers to interpret and use by employeed across an organization.
* The six stages of data processing are: Data collection, data preparation, data entry, preocessing, data interpretation and data storage.
* With the migration of big data to the cloud, companies make huge profits. Cloud technologies for big data allow companies to combine all their  platforms in a single, easy to adapt system.
* As software changes and updates, cloud technology seamlessly integrates the new with the old
* I found a very interesting video about Max Pooling in Convulutional  Neural Networks (https://www.youtube.com/watch?v=ZjM_XQa5s6s)

# 2020-04-26 #
* I have been busy preparing my last midterms, I haven't had enough time to read this week, but I hope I can do it after my last test.
# 2020-05-10 #
* I have been reading about the group project because I am not that sure about what we need to do
* I am excited about the group project!

# 2020-05-17 #
* I have been preparing my next midterms, they are very soon.
* I created a chat with my group members so we can discuss about the project; they joined but they have not replied yet. I am worried, its supposed to be a group project, hope they reply soon.

# 2020-05-31 #
* I have been working in the project with my group, I think we have been doing a great job.
* I have been preparing my finals as well, so I can feel the end of the semester.
* The first time I met some of my group members I did not feel that comfortable to be honest, two of them left; so now we are 3 in the group.
* The last time we met to work, I felt different, thats why I like team projects because you can learn from others, you can make new friends and you learn to be more tolerant with other people.
