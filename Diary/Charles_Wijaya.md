This diary file is written by Charles Wijaya E34085303 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* Watch the welcome video.
* Read success stories of AI.
* Find out that AI can be applied in almost all subject.
* AI can even interpret human emotion.
* Emotion interpretation is done by monitoring human facial expressions and body temperature.
* AI also gives solution to the farmer in the agriculture field.

# 2020-03-22 #
* AI has a lot of interpretation.
* People's perspective of AI is mostly affected by science fiction movie.
* AI is also divided into many parts, one of them is robot.
* The successful AI is different in each people opinion.
* Some experiments are done to test the AI intelligence.

# 2020-03-29 #
* There is still no announcement of what to do this week.
* Do a simple search about Phyton.
* Found out some usage of Phyton.

# 2020-04-05 #
* Watch the "But what is a Neural Network? | Chapter 1" from 2019 assignments.
* Trying to understand the content.
* Learn about the "Multilayer Perceptron".

# 2020-04-10 #
* See the website the teacher give in moodle.
* Read about basic programming in Phyton.
* Don't really unedrstand it, but I am trying hard to learn it.

# 2020-04-15#
* We did the synchronization teaching for the first time.
* Got to know more how the course might go on.
* See about what research the professor do.
* Read the 5.1 to 5.3 of the Machine Learning Basics.
* Many tasks can be solved with machine learning.
* Test set is done to a machine learning algorithm to test the algorithm if it is okay when deployed in real life.
* Algorithm is seperated to supervised and unsupervised algorithm.

# 2020-04-22#
* I am still trying to learn more about the Phyton.
* Learn how to use the print, help command.
* Plan to learn about the conditional statements and loops.
* Not really understand, but trying hard to learn.

# 2020-04-29#
* Watch the video by 3blue1brown videos.
* Learn more about the recognition of object in program.
* Know more about the group project we will do.

# 2020-05-06#
* Supposed to start doing the group project.
* Long time from starting the lesson, but still not really understand what is taught.
* Re-reading the previous materials, as I don't really understand it.
* See the example of the group project.

# 2020-05-13#
* Watch the introduction to handwritten digits.
* Read the Chapter 5.1-5.3.
* Watch another tutorial video about phyton in youtube.
* Read some success story from the google drive.

# 2020-05-20#
* Attend the first physical lecture.
* Learn that some things are better be done by human.
* A lot of AI has its own problem, not all is perfect.