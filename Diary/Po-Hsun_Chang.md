This diary file is written by Po-Hsun Chang E24086056 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* I knew the three applications of AI already.
* I learnt the reasons why AI is hard to be defined.
* I’ve never thought that asking a robot to pick up an item requires so much work.
* The success story of AI which indicates the AI dressing mirror surprised me.
* Applying AI to save wildlife did inspired me.
* I hope that the course material could be released on time on Wednesday afternoon.

# 2020-03-29 #

* I learnt about some basics of Python.
* I learnt printing and while-loop, for-loops, and so on. 

# 2020-04-04 #

* I learnt more about functions of Python.
* Meanwhile, I'm learing C, Java.
* I found out there're a few similarities in all programming languages.

# 2020-04-10 #

* Neutron holds an information or do tasks, ex. a number...
* Many neutrons that cause one another to fire is called neural network.
* Deep learning is the process of neurals doing tasks over and over again to find the best solution.
* It requires a lot of time and data to optimize deep learning.

# 2020-04-15 #

* TensorFlow is an end-to-end open source platform for machine learning.
* It scans tens of thousands of data to do machine learning.
* It reaches up to 98% accuracy in recognizing numbers.
* I don't really understand every step of the code he introduced.
* Attend the syncronizing class, and did a self-introduction.

# 2020-04-22 #

* Convolutional neural networks match parts of the image rather than the whole thing.
* About filtering: 1.line up feature and image patch. 2.multiply by each other. 3.add them up and devide it by the totoal number of pixels.
* Convolution layer:one image becomes a stack of filtered image.
* Pooling: take a 2X2 window, find the maximum value, track it.
* Normalization: When there's a negative value, change it to zero.
* Deep stacking makes the image gets more filtered.
* Couldn't access keras_01_mnist.ipynb

# 2020-04-27 #
* Using gradient function helps the computer define in which way way the cost function decreases the most rapidly.
* Gradient decent is a way to converge towards some local minimum of a cost function.
* The cost function reflects the complexity based on the a training example.
* The gradient of the cost funtion tells us what nudges to all of the weight and biaes cause thr fastest change to the alue of the cost function.
* To increase the activation of a nueron we can increase bias, increase the weight, or change the activations from the previous layer.
* Change every activations of the neurons and change the further layer.
* Backpropagation is the algorithm for the determine how a single training example would like to nungdge the weights and biases in terms of what relative proportions to those changes cause the most rapid decrease to the cost.

# 2020-05-10 #
* I contacted our group members.

# 2020-05-17 #
* If we want AI to evolve in a way that helps humans, then we need to define the goals and strategies that enable that path now.
* We should feed computers a more variety of human skin characteristis,including skin color,emotion and so on.
* A student used AI and 3D modeling to create photorealistic fake videos of people synced to audio.
* The computer collected so many photos and videos of a person,which are clear enough to see the wrinkles, to create such realistic fake video.
* The stop mechanism is like getting points, it works like accomplishing tasks. If gets more or equal points from shuttung down itself than finishing a task, It tend to shut itsulf down.

# 2020-05-24 #
* This week I participated in the physical class.
* We discussed a few questions about AI with different people.
* I finally found my groupmates and we've decided our group leader.
* We're going to have a discusstion about our final project next week.

# 2020-05-29 #
* I finally met all group members on Wednesday at McDonald's.
* we discussed and assigned work to each other.
* We will mett next Wednesday in class and do further discussion about the problems we came across.

# 2020-06-03 #
* I attended the physical class.
* My dream jon\b is to be an editor, and I don't think it will be replaced by machines or AI because it the job requires a lot of creativity.
* I learnt about industry evolution and we're still working on industry 4.0.
* The statistic showed the rate different jobs that have been replaced by AI. For example, check-in machines in at the airport, payment machines everywhere, and so on.
* Althogh some jobs are dissapearing for humans, there're more and more jobs we've never seen popping out, and what we have to do is to be prepared for it.
* In my knowledge, those jibs which requires lots of creativity or caring,deep feeling in our hearts are the least probable jobs to be replaced.
* Because it's human that have infinity imagination and creativity; it's human that can really know other's feeling, lnow what other's have experienced.

# 2020-06-13 #
* Our group met on Thursday, and we learn deeper about our code and about how it works.
* We finished the code, and started recording ourselves introducing the code.

# 2020-06-15 #
* We finished our group project and uploaded the vedio to youtube.