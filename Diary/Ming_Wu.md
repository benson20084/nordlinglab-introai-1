This diary file is written by MingZhen Wu F74076051 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* AI is not a countable noun because it is a scientific discipline.
* I wish to know more about how AI use those models to perform the task .
* All the AI methods we use today fall under narrow AI, with general AI being in the realm of science fiction.

# 2020-03-29 #

* I have a understanding of the basic principles of machine learning by reading the article this week.
* The functions and ideas behind neural networks are very cool. 
* I want to find out how machine learning to adjust the coefficients of those functions.

# 2020-04-05 #

* I learned to write programs to recognize handwritten numbers through the video.
* the question I raised last week is answered with the video from the material this week.
* How is it that scientists can come up with such a complex treatment of ML? 
* Tranflow is a really practical software for machine learing .

# 2020-04-12 #

* I learned to write programs with Python.
* The examples provided in the article of colaboratory are clear and easy to understand,thank you.

# 2020-04-19 #

* CNN is very different from KNN or the way above, but it is a new breakthrough.
* Writing ML program in Python is a subject that requires a lot of research.
* I'm not sure about the content and requirements of the assignments need to be handed in, so I'd like a more detailed explanation.


# 2020-04-26 #

* My learning objectives for this week are focused on gradient descent, the video goes into more depth on what it is..
* Most of the problems we need ML to solve can be divided into two parts, classification and identification.
* I think there should be a better way to achieve training goals than using gradient descent.
