This diary file is written by Winston Bendana F74077073 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

- The first week feels a little bumpy to be honest because of the covid-19 situation.  
- It was hard to upload my diary given that I wasn't invited to the repository yet, but once I got invited and accepted everything went smoothly.
- I did my success story based on what to me was a milestone in the machine learning comunity, so much so that a documenatary was based on it.
- It is very interesting to see how society reacts to a pandemic that came so suddenly and how even though technology has advanced a lot, transfering into a remote society is still complicated
- I really hope I can learn a lot from this course, I'm really interested in what machine learning can offer.

# 2020-03-29 #

- I finished the readings about the meaning of AI wich gave me an insight into how broad the concept of AI can be.
- One particular area of interest was machine learning and deep learning in wich machines are able to learn by experience.
- The thought experiments of the chinese room and the Turing test presented a new perspective into what actual intelligence is or might be.
- All in all we are reaching new horizons into what computers can do and automate, yet we're still very far from an artificial general intelligance capable of human or even super human levels of conciousness.

# 2020-04-23 #

- I already had previous experience with Python 3 but reading throught the google colab page refreshed my memory on the topics
- I watched the introduction to deep learning withought a PhD and it was very helpful to understand the different concepts
- I'm glad that the course material and flow is much more clear thanks to the online Q&A session
- Next week I'll start with the TensorFlow practial stuff, I hope that understanding the concepts gives me an esier time on the practical learning

# 2020-05-02 #

- Started to use the Google Colab and learned how Jupyter Notebooks work and how to run python code with them
- I was introduced to the concepts of Neural Networks, activation functions, loss functions, and non linerearity
- The MINST dataset has been very good to demonstrate how Neural Networks act on vectors of data

# 2020-05-09 #

- Started to tinker with Keras librrary to make Machine Learning models more easely
- It's interesting how all of these so-called "hyper parameters" affect the model accuracy in different ways
- Now that I know who my group members are we can start to talk about how are we doing our final project

# 2020-05-28 #

- These past few weeks I've learned mostly about how to deal with bias and how to have better data to feed my model.
- We also made a strategy with my group to make the final project and should start doing it next week
- I find it interesting how developing a machine learning model is more about finding the right data and adjusting the hyperparameters rather than making some discrete algorithm.
- I also realize that most Deep Learning projects need different amount of neurons, layers, and data because of the way Neural networks work.
