This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Cornelia Hanimann E14087034 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.


# 2020-05-30 #

Today I finally do my first diary entry. 
Previously I joined a few online classes and have no experience with coding and machine learning. 
Today me and my Teammates tried to do the TensorFlow tutorial and also watched a few videos about machine learning. 
We asked a few of our friends for help and had a few issues with errors.
We spent some time with the set up and tried to uderstand what the project is about, so hopefully we'll have something of substance next week. 


And my teammates told me that there's such a thing as fish-rain. 
The more you know...

# 2020-03-19 #

This week I only watched the video from our teacher that was uploaded to Youtube. Afterwards I looked at the link he shared with us. 
The link informed me about the definition of AI, the ethical aspects of artificial intelligence and in which fields AI is useful. 
Later I saw that we had do the set up with Python, which I didn't really bother with. 
The problem was, that I realised everyone already has some understanding of coding except for me. 
I need someone to explain the simple concepts to me and since this class  never really took place I felt like I can't talk to the teacher either. 


# 2020-04-02 #

I've watched this channel before and it's really good at making things interesting about mathematics and these anomalies in math. 
My bigger issue with this is that I sometimes leave feeling like I've learned something while in reality I've likely already forgotten it the next time. 

It's a good video though. 

# 2020-06-03 #

Classes are actually a lot more engaging and fun when you understand what the teacher says. 
Because of the Corona-Virus, I rarely spent time in class and since all my other classes are in chinese, I am just doing it for attendance .
Today I felt like there was a point to going to class again, as if my presence was not for the university but for me. 
We have discussed how automation will eventually replace jobs. I am a mechanical engineering student and while I was always under the impression that this is a replaceable job
because the world produces this many engineers, today I was told that my goal is not that replaceable. 
Originally I did an apprenticeship as a car mechanic, so I always have the option to just return to the job of fixing machinery. 
Now it turns out I even have a future, so that's nice. In terms of the replacement issue, I believ in the US ex candidate Mr. Wang talked about that
he said something about countering that problem by givine people a monthly income or something. Unfortunately he does not run for president anymore. 

I had these conversations before and I think that we will lose jobs but also find new jobs in the future. 



