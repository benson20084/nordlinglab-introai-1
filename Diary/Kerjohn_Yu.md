This diary file is written by Yu KerJohn F74077023 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #
* Had quite a difficult time figuring out how to write a diary
* there are many fields and technologies that are related to AI
* i have learned  new key terminologies
* Machine learning is a subfield of AI

# 2020-03-28 #
* Deep learning is a subfield of machine learning, which itself is a subfield of AI, which itself is a subfield of computer science(explains how all connects to one another)
* Data Science can be applied in business and science by using algorithms, data storage and web application development
* there are many robots around us but they are considered as software robots (which aren't actually robotics)
* the Turning test is an imitation game that test the robot whether if it reached human-level intelligence 
* the intelligent behavior of the self driving car's system is fundamentally different from actually being intelligent as it isnt able to have different feel like humans while driving 

# 2020-04-05#
* learned a little bit about the python programming language
* i learned that python is a language that automates memory management which means that the memory is cleared and freed immediately unlike C language
* Python has a simple and clear syntax which makes it more understandable
* python has many built in function which makes life alot easier.

# 2020-04-12#
* what is a neural network? which is a set of algorithm used in machine learning
* i learned that they used specific function to determine of a picture is as close as the original value
* at the end of the video, it tells us that Sigmoid is a slow learner compared to the new RELU
* there is unsupervised learning algorithm and supervised learning algorithm that associates examples with label or target
* The no free lunch theorem shows that the more the training sets, it will lead to lesser test errors as there are fewer incorrect hypotheses

# 2020-04-19#
* i have went through the google colab about python programming basics and also Tensorflow.
* I watched the video about the introduction to recognition of handwritten digits using the MNIST example in "TensorFlor and Deep Learning without a PhD" by Martin Gorner.
* TensorFlow was built with open source and ease of execution and scalability in mind. It can be run in the cloud, but also locally.
* i recently started watching youtube tutorials about python such as tuples , libraries and stuff that aren't similar to other programming language 

# 2020-04-26#
* i watched the tutorial about TensorFlow and Deep learning on Youtube(TensorFlow and Deep Learning without a PhD, Part 1 (Google Cloud Next '17))
* it briefly taught me about the lines of code that is needed to run test and training for recognzing numerical digits from batches of images.
* GradientDecentOptimizer is used to let the program run to a certain accurary which trains it.
* we have to provide the neural network with batches of images to train with to incrase the accuracy and decrease the cross_entropy.
* i also learned that in the video there are two types of ways to process the images which are flatenning the images into multiple pixes or do a convolutional layers.
* in the video it seems that using the dropout method(shooting off some of the pixels to reduce degree of freedom) works only during the convultional layers.

# 2020-05-03#
* i started on Introduction to data preprocessing, model selection, regularization, activation functions, and neural network types, including convolution
* the Convolutional Neural Network match pieces of the image which is filtering and repeat will all of the types of features to obtain a stack of filtered images (convolution layer)
* There is also pooling(shrinking the images stack) by taking the maximum value of a set of pixels.
* Normalization(negative values to zero) which is a ReLu Layer
* each of them are input of other's output (Convolution,ReLu,Pooling)
* Gradient descent ( adjusting up and down to see how the error changes) error= right answer-actual answer 

# 2020-05-10#
* read through some stuff about the Juypter tool and download it on my laptop.
* today i saw some of the classifier made by others from http://yann.lecun.com/exdb/mnist/ that shows how the preprocessed the images/datasets.
* trying to search for my teammates this week on facebook.

# 2020-05-17#
* started the Ethics and dangers of alogrithm bias topic
* formed a Line group with my team members in Line 
* started to do a little exercies on Tensorflow 
* watched  How we can build AI to help humans, not hurt us, Fake videos of real people -- and how to spot them and many other more during the weekend 

# 2020-05-24 #
* attended this week's lecture
* algorithm bias and also general artificial intelligence is a very interesting topic
* this week my group and i will be discussing for the group project

# 2020-05-31 #
* our group have divided the work and started to work on our parts for the projects
* currently trying to learn about the algorithm of the project using https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/#4
* besides that , i'm currently preparing for my projects for other courses as well
* looking forward to having lecture this upcoming week to attend the lecture and have further discussion with my teammates

# 2020-06-07#
* attended lecture this week
* i will most probably be buying and electric car as my first car
* the lecture was intresting and it taught me tons of general knowledge that are usefull
* my dream job is apparently the job that removes jobs from others 
* hopefully my group will be able to finish our project ASAP.
* there were fewer people in the class, most probably due to the final's month

# 2020-05-14#
* finished with the machine learning code with my teammates
* explained how the code works to my teammates for the recording of the video
* cant wait for summer holiday to start
* currently preparing for all my final exams 
* cant wait for next week's lecture and presentation from other group's 