This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Hsiao_Ting Fu S58084054 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture was great.
* I wish to know more about ImageNet.
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* I don't think exponential growth applies to food production.
* Why doesn't my doctor use an AI.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I knew Python.
* I wish the Python exercise was a bit more challenging.
* I learnt the difference of `print` in Python version 2 and 3.
* I love the [Learn Python the hard way](https://learnpythonthehardway.org/) book and recommend it to everyone. 
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) i

# 2020-3-12 #
* The first lecture was great.
* I have read the aritcle about AI in the material file and got some basic knowledage.
* AI could apply on almost any fields to help humans to calculate and process stuffs. 

# 2020-3-19
* The second lecture was the introduction of python. 
* I am just learning C++ programming, have no much experience on coding or programming. 
* I think it is a bit hard to learn more than one programming language, it is easier to learn pythong for me.
* The instruction on the web is very clear.

# 2020-4-2
* The third lecture was talking about neural neural network.
* The seriers video maked by 3blue1brown group was very good, easy to understand the theory of neural network.

#2020-5-14 
*Q1: Is bias really that bad in algorithm of AI? Biases do exist in different situations and come from the different population, society, culture, religion, etc.  Does a universal algorithm fix all people have different background and culture?
*Q2: photorealistic fake videos of people might make it possible to having conversation with the dead people. However, this is not real. Does this ethic? Making fake videos to speaking something that might be not from speaker’s will. 
*Q3: Who has right to see these bigdata and who can decide what dataset put to train AI system? Governments or industry companies or the owner of robot? 

