This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Martin Wu E1000000 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture was great.
* I wish to know more about ImageNet.
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* I don't think exponential growth applies to food production.
* Why doesn't my doctor use an AI.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I knew Python.
* I wish the Python exercise was a bit more challenging.
* I learnt the difference of `print` in Python version 2 and 3.
* I love the [Learn Python the hard way](https://learnpythonthehardway.org/) book and recommend it to everyone. 
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2020-3-20 #

* My name is Chen Wei Su. My student ID is C24061038. I major in physics.
* I watched a series of videos with 4 lectures by 3Blue1Brown,talking about neural networks, recap, gradiant descent, backpropagation and so on in both concpetual and mathematical way. They are quite interesting.
* I aequire some valuable information in SuccessStories abour several aspects of how AI is applied in daily life. I prefer the case talking about AlpgaGo.
* There is also a course of AI in research institute of physics. I get some ppt as my additional reading.

# 2020-3-28 #

* I read some introduction about AI's apllication used in high energy physics and how to improve data analysis with AI.
* I also read AI's application used in astrophysics.
* I watch a video about convolutional network for success story introducing AI's application in detecting gravitational lenses. 

# 2020-4-03 #

* I watch a documentay talking how aplphaGO challenge Go player and show its power that human can not achieve.
* AlphaGo is quite mighty that it explores the unknown potential that Go player deserves to keep studying into the Go.

# 2020-4-11 #

* I watch a video as a brief introduction of convolutional network.
* Convolutuonal network is quite unexpected for me that its algorithm can focus on local features among pictures.

# 2020-4-18 #

* On-line course has finaaly started this week. Course material is comprehensive, but it sometimes is not too easy.
* Mid-term exam is coming. I have to spend more time on my major courses.

# 2020-4-25 #

* I learn Python with course material, it is easy to be understood.
* Python is quite easy compared with c++. 

# 2020-5-03 #

* I discuss some problems about Python with my seniors.
* I start using Jupyternotebook and Spyder to pratice Python on the website.
