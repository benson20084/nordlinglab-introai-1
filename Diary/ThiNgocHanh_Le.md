This diary file is written by ThiNgocHanh Le P78087037 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
Since I wait for the video but I do not see on the youtube. So I missed writing diary for three weeks.
I took note my lesson in my .ppt file and I commit in the bitbucket from this week. 

# Lesson of week 2020-03-12 #
* Conduct the tasks related to this course: sign up to get access the material of this course, take the surbey about my background in the Google Form.
* I read the ppt about the Success stories deep learning. Through the Overview of the sucess stories of Deep learning, I know more about:
- Problems deep learning can solve well
- Knowledge related to deep learning: dataset, GPU computing, training

# Lesson of week 2020-03-19 #
* Read the sucess story in the folder SucessStories in the Goodle Drive.
* Find a success story that I am interested in.
* Prepare ppt about a success story. My topic is "Bank loan approval process
* Read the ppt "What is AI, DL, Data Science, Data Mining, Statistics,and System Identification"

# Lesson of week 2020-03-26 #
* Explore about Python
Follow the sample code provided by Professor on colab, I practice with MNIST data on my computer and I compile with Pycharm.

# Lesson of week 2020-04-02 #
Practice Python on Jupyter notebook
* print() function
* string in Python
* Conditional statements and loops
* Practice about the data types in Python. I take note about the diffent with C++. Since I am familiar with C++.
* Classes and Modules
* Work with files: reading and writing files. I practice working with .txt file and .csv file.

# Lesson of week 2020-04-09 #
* Watch the video "But what is Neural Network?"
- Through the video, I know more about the layers. How to calculate at layer. 
- Parameters at layers
- Sigmoid function. Why it is used in neural network and how it works.
* Read chapter 5 in deep learning book.
# Lesson of week 2020-04-16 #
* Attend the Synchronous online lectures.
* Explore Tensorflow: I watch the video and read the ppt to understand how we employ tensorflow in deep learning:
- Know the MNIST dataset.
- The architachture of a simple model in classification.
- Know about bias, weight, flaten.
- Know what is softmax function and how it work to find the classification output.
- Know how to use tensorflow to demonstrate the computation in deep learning problem.
* I practice tensorflow with the MNIST for the classification problem. I explore the file "mnist_1.0_softmax.py" on github.

# Lesson of week 2020-04-23 #
*I watch the video "How Convolutional Neural Networks work" and read chapter 9 in Dêp learning book. I understand about:
- Filter: what is filter and why we use filter. There are different filter size, such as 3x3, 5x5, 7x7. I try to practice by utilizing different filter to understand more.
- Convolution: how does it work and output after apply convolution layer
- Pooling: I explore different type of Pooling, such Max Pooling, Average Pooling. I practice on them.
- Activation function: In the video, they introduce ReLu. I try to use another one: sigmoid
- Fully connected: I explore input and output of this kind of layer.
# Lesson of week 2020-04-30 #
I watch the video and read the chapter 3 in deep learning book. What I've learned are:
* Inside the network, how the weights and bias work.
* Loss function
* Backpropagate
* I look for some deep learning based paper to support this course.

# Lession of week 2020-05-07#
* Find a paper that is used deep learning based method. I try to find an article related to my research. That is, deep learning method to generate a saliency map.
* Think about the final project.
* I practice more on loss function.
# Lession of week 2020-05-14 #
* I keep going practice my python code on deep learning.
* I watch the videos about the Ethics in deep learning.
* I think about the project to conduct the project.

# Lession of week 2020-05-21 #
* I practice on MNIST dataset to understand more about loss function.
* I read an article about training of Deep Neural Networks using supervised learning as the requirement of this course.

# Lession of week 2020-05-28 #
I attended the class on 05/28:
* By discussing with classmates and Prof, I know more about other dream work in next 10 years.
* Through the lession from Prof, I know more about the evolution 4.0 and how AI influences to our life.
* I keep going practicing Python with deep learning.

# Article Summary #

I read the paper "Image similarity using Deep CNN and Curriculum Learning". Link: https://arxiv.org/ftp/arxiv/papers/1709/1709.08761.pdf
The following is what I've learn from this paper.
* A network is utilized to define the similarity between two image. That is Siamese Network.
* How to extract feature through layers.
* How to share feature between two architectures.
* How to build the Loss function to define the similarity between two image. 
* CIFAR 10 dataset is used in this study, which consists of 60.000 images belongs to 10 different classes.

# Lession of week 2020-06-04 #
* I attended the class on 06-04. Discuss with my classmates about our dream work and how the automation impacts to our dream work. Besides, Prof. Norlding introduce the future work.
* I keep going conduct my final project.

