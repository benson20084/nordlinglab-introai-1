This diary file is written by Po Ki Cheng H14088058 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-29 #

* I had some troubles regarding to the course enrolment. Luckily, I can now finally solve it and add this course successfully.
* This week, I just focused on the course materials available on moodle and figured out how the bitbucket works.
* I also read the materials of AI success stories which are very inspiring.
* I realized AI is closed related to ou daily life but not just about robots and other high-tech stuff.
* AI's usage is very wide, affecting not only the IT industry but also our entertainment, such as NBA, Pop music, or even transportation like plane.
* I am excited to learn more about AI, DL, most importantly, introduction to Python.

# 2020-05-03 #

* I just noticed that there is synchronized lectures in recent weeks. I did not receive any email and neither did I check moodle that frequently, which is my bad.
* I had the very first synchronized lecture and I found it quite effective that we could directly ask the Prof questions and have more interaction.
* Since I missed some lectures, I could not catch up with part of the class. Thats too bad!
* But then I tried to read through all the reading materials available and understand more about it after class.
* I definitely need to put more effort on catching up with the progress. 
* It is quite a pity that we could not have synchronized class earlier and everything was quite confusing. I wish I could start the course all over again. :(