# 2020-03-21 

Lecture 1: Artifitial intelligence can be used for:
---

*Face recognition

*Compare images

*Diagnose breast cancer

*Create ingredients

*Capture emotional reactions

*Teaching 

*Wildlife conservation

*Analize crime scenes

*Selfdriving cars

*AI can process more information than humans

----------------------------------------------
Lecture 2: What is AI, Deep Learning, and Machine Learning?
----------------------------------------------
What is AI?

*Use autonomy and adaptivity to define AI

*AI is a scientific discipline

*Use "an AI method" instead of "an AI"

Related fields:
-

*Machine lerning (subfield of AI)

*Deep learning (subfield of machine learning)

*Robotics: building and programming robots

*Data science (machine learning and statistics)

-

# 2020-03-19

Lecture 3: Introduction to Python
-

*Python is a programming language

*In python we use  cells to separate the codes

*We can show texts, graphics, calculations, etc

*Codes are similar but simpler than C codes

------------------------------------------------------------------------

# 2020-03-26

Lecture 4: Introduction to Python
-

*I install on my computer the python program to practice

-

------------------------------------------------------------------------

# 2020-04-01

Lecture 5:Introduction to Neural Networks and Modeling 
-

*Neural networks is a structure to let the machine regcognize something.

*Neurons form layers. From 2 to infinite number of layers. (The ones in between are the hidden layers)

*Use equations to get numbers from each pixel of a picture and give the resulting number to a neuron

------------------------------------------------------------------------

# 2020-04-08

Lecture 6: Introduction to TensorFlow 
-

Steps to write codification with tensor flow

*Initialization

*Model

*Success metrics

*Training step

*Run


Basic methods to correct the neural networks:

*softmax cross-entropy mini-batch, 

*learning rate decay (to lower noise), 

*dropout (for regularization), 

*overfitting (due to a)bad network, b)too many neurons, c)not enough data)

*convolutional layers (to condense information)


-------------------------------------------------------------------------

# 2020-04-15

Lecture 7: Introduction to data preprocessing, model selection, regularization, activation functions, and neural network types, including convolution 
---

Filtering

*Line up the feature and the image path

*Multiply each image pixel by the corresponding feature pixel

*Add them up

*Divide by the total number of pixels in the feature

Convolution 

*Try every possible match

-------------------------------------------------------------------------

# 2020-04-22

Lecture 8: Gradient descent, Backpropagation, and loss functions 
-
 
How the let the AI learn?

*Show it a number we identify.

*If He gives probabilities of several numbers we show it matematically was number should be by the COST of a single practice

for the incorrect number:(number of the computer probability - 0.0)^2

for the correct number: (number of the computer probability - 1.0)^2

Here we have small(he is right)and big(he is doing it wrong) numbers.

*With derivatives and gradient we can calculate the lowest cost, and so change the weights and biases to get lower error.



How the let the AI learn?

*Show it a number we identify.

*If He gives probabilities of several numbers we show it matematically was number should be by the COST of a single practice

for the incorrect number:(number of the computer probability - 0.0)^2

for the correct number: (number of the computer probability - 1.0)^2

Here we have small(he is right)and big(he is doing it wrong) numbers.

*With derivatives and gradient we can calculate the lowest cost, and so change the weights and biases to get lower error.

--

# 2020-04-29

Lecture 9: Introduction to Model validation
--

*test/training split: is for splitting a single dataset for two different purposes: training and testing.

*cross-validation: Cross-validation is a technique that is used for the assessment of how the results of statistical analysis generalize to an independent data set. Cross-validation is largely used in settings where the target is prediction and it is necessary to estimate the accuracy of the performance of a predictive model.

*overfitting: Overfitting is a modeling error that occurs when a function is too closely fit to a limited set of data points. Overfitting the model generally takes the form of making an overly complex model to explain idiosyncrasies in the data under study.

*Underfitting: occurs when a statistical model or machine learning algorithm cannot capture the underlying trend of the data. Intuitively, underfitting occurs when the model or the algorithm does not fit the data well enough. Specifically, underfitting occurs if the model or algorithm shows low variance but high bias.

*bias variance tradeoff: is the property of a set of predictive models whereby models with a lower bias in parameter estimation have a higher variance of the parameter estimates across samples, and vice versa.



--

# 2020-05-07

Lecture 10: Aplication of deep learning
--

*I choose to make the final proyect by myself instead of in a group. I kind of regret that now due to is a bir long. I will try to do my best anyways.

*Until now I already have the code with some data I dowload it to make the AI learn. Is a recopilation of several images of hand written numbers.

*I am trying the different ways like convolution and LU to decrease the error of my codes.

*Only the jupyter notebook and preparation for the presentation left to do.


------------------------------------------------------------------------

# 2020-05-21

Lecture 13: Automation and the future of work
--

I believe Artifitial Inteligence is quite helpful nowadays, however is important to have boundaries and not let AI do everything for us.

Personaly, I believe the AI in the chemical engineering chemistry can atribute to me in modeling, process control, classification, fault detection and diagnosis.



